const express = require('express');
const ReactDOMServer = require('react-dom/server');
const React = require('react');
const Message = require('./../ui/js/component/Message').default;

let app = express();

app.get('/', function (req, res) {


    let innerHtml = ReactDOMServer.renderToStaticMarkup(<h1>Pass me around</h1>);

    let content = ReactDOMServer.renderToString(
        <Message text="Initial text" toBeHydrated="1">{innerHtml}</Message>
    );

    let html = `
        <!DOCTYPE html>
        <html>
            <head></head>
            <body>
                <div id="render-me-client"></div>
                <div id="message">${content}</div>
                <script type="text/javascript" src="/static/js/main.js"></script>
            </body>
        </html>
    `;

    res.send(html);
});

app.use('/static', express.static('/var/www/app/public'));

let port = 3000;

app.listen(port, () => console.log(`Example app listening on port ${port}!`))