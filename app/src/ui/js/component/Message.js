const React = require('react');

class Message extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            text: props.text ? props.text : ''
        };

        this.onTextChange = this.onTextChange.bind(this);
    }

    onTextChange(e) {
        this.setState({
            text : e.target.value
        });
    }

    render() {

        let serverProps = "";

        if (this.props.toBeHydrated) {

            const reactProps = JSON.stringify(this.props);

            serverProps = <div data-react-props={reactProps}></div>;
        }

        return (<div>
            <label>Text</label>
            <textarea name="text" onChange={this.onTextChange} value={this.state.text}></textarea>
            {this.props.children}
            {serverProps}
        </div>);
    }
}

exports.default = Message;