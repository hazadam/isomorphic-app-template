const React = require('react');

class ClientMessage extends React.Component {
    render() {
        return <h2>Hello from React at Client side</h2>
    }
}

exports.default = ClientMessage;