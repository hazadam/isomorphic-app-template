const React = require('react');
const ReactDom = require('react-dom');
const ClientMessage = require('./component/ClientMessage').default;
const Message = require('./component/Message').default;

ReactDom.render(<ClientMessage></ClientMessage>, document.getElementById('render-me-client'));

let messageElement = document.getElementById('message');
let reactProps = messageElement.querySelectorAll('[data-react-props]')[0].getAttribute('data-react-props');
reactProps = JSON.parse(reactProps);

ReactDom.hydrate(<Message {...reactProps}></Message>, document.getElementById('message'));