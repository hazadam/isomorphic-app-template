module.exports = env => {
    return {
        mode: 'development',
        optimization: {
          minimize: false,
        },
        devtool: "source-map",
        entry: './src/ui/js/client.js',
        output: {
          path: env.JS_PUBLIC_DIR
        },
        module: {
            rules: [{
                test: /\.(js)$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                      presets: ['@babel/preset-react']
                    }
                  }
            }]
        }
    }
};